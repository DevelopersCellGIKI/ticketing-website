'use strict'

const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const organizerModel = require('../models/organizerModel');
const secret = require('../configs/authConfig').secret;

// middleware for token verification
module.exports = function(req, res, next) {

    // extract token
    let token = req.params.token || req.body.token || req.query.token || req.headers['x-access-token'];
    // decode
    if(token) {
        // verify token
        jwt.verify(token, secret, function(err, decoded) {
            if(err) {
                res.send({success: false, message: 'Failed to authenticate token.'});
            } else {
                // on successful verification, set req variable(decoded) with paylod
                req.decoded = decoded;
                console.log(decoded);
                // goto next middleware
                next();     
            }
        });
    } else {
        // if no token
        res.send({success: false, message: 'No token was provided'});
    }
}