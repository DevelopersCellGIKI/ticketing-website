'use strict';

const organizerModel = require('../models/organizerModel');
const validator = require('validator');
const bcrypt = require('bcrypt-nodejs');
const jwt = require('jsonwebtoken');
const secret = require('../configs/authConfig').secret;

module.exports = class organizerService {

    /* ==================
     Register Organizer
  ================== */

    register(data, callback) {
    
            //let check = true;

            // validate username
            var name = validator.trim(data.name);
            if(!name) {
                callback(null, { success: false, message: 'name can not be empty' });
                return;
            } else {
                // name cant contain any special characters
                var nameRegExp = /^[a-z ,.'-]+$/i;
                if(!validator.matches(name, nameRegExp)) {
                    callback(null, { success: false, message: "Name \'" + name +"\' not in correct format"});
                    return;
                }
            }

            // validate email 
            var email = validator.trim(data.email);
            if(!email) {
                callback(null, { success: false, message: 'Email can not be empty' });
                return;
            } else {
                if(!validator.isEmail(email)) {
                    callback(null, { success: false, message: "Email \'" + email +"\' not in correct format"});
                    return;
                }
            }

            // validate password
            var password = validator.trim(data.password);
            if(!password) {
                callback(null, { success: false, message: 'Password can not be empty' });
                return;
            } else {
                if(!validator.isLength(password, {min:8, max:35})) {
                    callback(null, { success: false, message: 'Password must be atleast 8 characters but no more than 35'});
                    return;
                } else {
                        // encrypt password
                        var hash = bcrypt.hashSync(password);
                        console.log('at end');
                    }
                }
                
                let organizer = {
                    name: name,
                    email: email,
                    password: hash
                };

                organizerModel.create(organizer).then((result) => {
                    if(result) {
                        // create token for activation, expiray time => ('24h') 
                        var token = jwt.sign({email: email}, secret, {expiresIn: '24h'});
                        // on success, send token for activation
                        callback(null, {success: true, message: 'User registered', token: token});
                    } else {
                        callback(null, {success: false, message: 'User on this email already registered'});
                    }
                }).catch((err) => {
                    callback({ success: false, message: err }, null);
                });
        }

    /* ==================
     Activate Organizer
  ================== */

     activate(email, callback) {
         // get user by email
         organizerModel.getByEmail(email).then((organizer) => {
             if(!organizer) {
                 callback(null, {success: false, message: 'No user found for activation'});
             } else {
                 // if already activated
                 if(organizer.dataValues.active) {
                     callback(null, {success: false, message: 'Account is already activated'});
                    } else {
                        // activate user
                        organizer.active = true;
                        // update in db
                        organizer.save({fields: ['active']}).then(() => {
                            callback(null, {success: true, message: 'Account activated!'});
                        }).catch((err) => {
                            callback(null, { success: false, message: 'Unable to activate user in database.' });
                        });
                    }
                }
         }).catch((err) => {
             callback({ success: false, message: err }, null);
         });
     }


    /* ==================
     Login Organizer
  ================== */

     login(data, callback) {

             // validate email 
            var email = validator.trim(data.email);
            if(!email) {
                callback(null, { success: false, message: 'Email can not be empty' });
                return;
            } else {
                if(!validator.isEmail(email)) {
                    callback(null, { success: false, message: "Email \'" + email +"\' not in correct format"});
                    return;
                }
            }

             // validate password
            var password = validator.trim(data.password);
            if(!password) {
                callback(null, { success: false, message: 'Password can not be empty' });
                return;
            }
            
            // get user by email
            organizerModel.getByEmail(email).then((organizer) => {
                if(!organizer) {
                    callback(null, { success: false, message: 'Email not found' });
                } else {
                    // compare passwords for login
                    var validPassword = bcrypt.compareSync(password, organizer.dataValues.password);
                    if(!validPassword) {
                        callback(null, { success: false, message: 'Incorrect Password' });
                    } else {
                        // if not activated
                        if(!organizer.dataValues.active) {
                            callback(null, { success: false, message: 'Account is not yet activated' });
                        } else {
                            // create token for user login
                            const token = jwt.sign({userId: organizer.dataValues.id},
                                                    secret, {expiresIn: '24h'});
                            // on success, send user info and token
                            callback(null, {
                                organizerId: organizer.dataValues.id,
                                name: organizer.dataValues.name,
                                success: true,
                                message: 'Successful login!',
                                token: token
                            });
                        }
                        
                    }
                }
            }).catch((err) => {
                callback({ success: false, message: err }, null);
            });
        }


    /* ======================
     Resend Activation Link
  ====================== */

        resendLink(data, callback) {

            // validate email
            var email = validator.trim(data.email);
            if(!email) {
                callback(null, { success: false, message: 'Email can not be empty' });
                return;
            } else {
                if(!validator.isEmail(email)) {
                    callback(null, { success: false, message: "Email \'" + email +"\' not in correct format"});
                    return;
                }
            }

            // get user by email
            organizerModel.getByEmail(email).then((organizer) => {
                if(!organizer) {
                    callback(null, { success: false, message: 'Email not found' });
                } else { 
                    // if account activated
                    if(organizer.dataValues.active) {
                        callback(null, { success: false, message: 'Account is already activated' });
                    } else {
                        // create token for activation
                        var token = jwt.sign({email: organizer.dataValues.email}, secret, {expiresIn: '24h' });
                        // on success, send token for activation
                        callback(null, {
                                success: true,
                                message: 'Activation link has been sent to your email',
                                token: token
                            });
                    }
                }
            }).catch((err) => {
                callback({ success: false, message: err }, null);
            });
        }


     /* ================
     Forgot Password
  =============== */


        forgotPassword(data, callback) {

            // validate email
            var email = validator.trim(data.email);
            if(!email) {
                callback(null, { success: false, message: 'Email can not be empty' });
                return;
            } else {
                if(!validator.isEmail(email)) {
                    callback(null, { success: false, message: "Email \'" + email +"\' not in correct format"});
                    return;
                }
            }

            // get user by email
            organizerModel.getByEmail(email).then((organizer) => {
                if(!organizer) {
                    callback(null, { success: false, message: 'Email not found' });
                } else {
                    // if not activated 
                    if(!organizer.dataValues.active) {
                        callback(null, { success: false, message: 'Account is not activated' });
                    } else {
                        // create token for reset password
                        var token = jwt.sign({email: organizer.dataValues.email}, secret, {expiresIn: '24h' });
                        // on success, send token for reset password
                        callback(null, {
                                success: true,
                                message: 'Reset password link has been sent to your email',
                                token: token
                            });
                    }
                }
            }).catch((err) => {
                callback({ success: false, message: err }, null);
            });
        }
    
    /* ================
     Change Password
  =============== */

    changePassword(data, callback) {

        var email = data.email;

        // validate password
        var password = validator.trim(data.password);
        if(!password) {
            callback(null, { success: false, message: 'Password can not be empty' });
            return;
        } else {
            if(!validator.isLength(password, {min:8, max:35})) {
                callback(null, { success: false, message: 'Password must be atleast 8 characters but no more than 35'});
                return;
            } else {
                    // encrypt password
                    var hash = bcrypt.hashSync(password);
                }
            }

            // get user by email
            organizerModel.getByEmail(email).then((organizer) => {
                if(!organizer) {
                    callback(null, {success: false, message: 'No user found'});
                } else {
                    // if not activated
                    if(!organizer.dataValues.active) {
                        callback(null, {success: false, message: 'Account is not yet activated'});
                    } else {
                        // change password with new password
                        organizer.password = hash;
                        // update password in db
                        organizer.save({fields: ['password']}).then(() => {
                            callback(null, {success: true, message: 'Password changed successfully!'});
                        }).catch((err) => {
                            callback(null, { success: false, message: 'Unable to change password in database.' });
                        });
                    }
                }
            }).catch((err) => {
                callback({ success: false, message: err }, null);
            });
        }

    }
