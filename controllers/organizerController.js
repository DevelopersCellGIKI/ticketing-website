const express = require('express');
const router = express.Router();
const organizersServices = require('../services/organizerService');
const organizer = new organizersServices();
const authenticate = require('../middlewares/authenticate');

module.exports.construct = function () {

    router.post('/register', function(req, res) {
        organizer.register(req.body, function(err, response){
            if(err) {
                res.send(err);
            } else {
                res.send(response);
            }
        });
    });

    router.put('/activate/:token', authenticate, function(req, res) {
        // extract email from decoded payload
        var email = req.decoded.email;
              organizer.activate(email, function(err, response) {
                if(err) {
                    res.send(err);
                } else {
                    res.send(response);
                }
            });
      
    });

    router.post('/login', function(req, res) {
        organizer.login(req.body, function(err, response) {
            if(err) {
                res.send(err);
            } else {
                res.send(response);
            }
        });
    });

    router.post('/resend', function(req, res) {
        organizer.resendLink(req.body, function(err, response) {
            if(err) {
                res.send(err);
            } else {
                res.send(response);
            }
        });
    });

    router.post('/forgotpassword', function(req, res) {
        organizer.forgotPassword(req.body, function(err, response) {
            if(err) {
                res.send(err);
            } else {
                res.send(response);
            }
        });
    });

    router.put('/changepassword/:token', authenticate,function(req, res) {
        
        var data = {
            email:    req.decoded.email,    // extract email from decoded payload
            password: req.body.password     // extract password from body
        };
        
        organizer.changePassword(data, function(err, response) {
            if(err) {
                res.send(err);
            } else {
                res.send(response);
            }
        });
    });



};

module.exports.router = router;