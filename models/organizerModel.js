'use strict';

const db = require('../configs/db');
const Sequelize=require('sequelize');
const sequelize = db.sequelize;
const Op = Sequelize.Op;

class OrganizerModel {
    constructor() {
        this.Organizer = sequelize.define('Organizers', {
            name: {
                type: Sequelize.STRING, allowNull: false
            },
            email: {
                type: Sequelize.STRING, unique: true, allowNull: false
            },
            password: {
                type: Sequelize.STRING, allowNull: false
            },
            active: {
                 type: Sequelize.BOOLEAN, allowNull: false, defaultValue: false
            }
        }, {
            timestamps: false,   // don't add the timestamp attributes (updatedAt, createdAt)
            freezeTableName: true   // disable the modification of table names
        });

        // Create the table
        this.Organizer.sync().then(() => {
            console.log('Organizers Table Created');
        });
    }

    // create organizer in database (if email not already exists)
    create(data) {
        return this.Organizer
            .findOrCreate({where: {email: data.email}, defaults: data})     // ensure unique email
            .spread((organizer, created) => {
                console.log(organizer.get({
                    plain: true
                }));
                return created;     // true if new organizer was created else false

            }).catch(err => err);
    }

    // get user by email
    getByEmail(email) {
        return this.Organizer.findOne({ where: {email: email}})
            .then(organizer => {
                return organizer;
            }).catch(err => err);
    }

}

module.exports = new OrganizerModel();